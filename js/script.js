window.addEventListener("DOMContentLoaded", () => {
    let masterGoodsDiv;
    let pageNav;
    let serverAddress = "http://192.168.31.35:8080/"
    let token = '';

    async function getToken() {

        if (token == '') {
            let cookies = await document.cookie.split(";");
            for (let i = 0; i < cookies.length; i++) {
                if (cookies[i].startsWith("token=")) {
                    token = cookies[i].substring(6)
                }
            }
        }

        if (token !== '') {
            return token;
        } else {

            let response = await fetch(serverAddress + "api/user", {
                method: 'GET',
                credentials: 'include'
            })
            const json = await response.json();
            token = await json.token;
            document.cookie = 'token=' + token;
            return token;

        }

    }

    function init() {
        const srv = new XMLHttpRequest();
        srv.open("GET", serverAddress + "api/animalTypes");

        srv.send();

        srv.addEventListener("readystatechange", () => {

            if (srv.readyState == 4 && srv.status == 200) {
                const data = JSON.parse(srv.response);


                data.forEach(element => {
                    const span = document.createElement("span");
                    span.classList.add("span-nav");

                    let buttonList = [];

                    element.companyList.forEach(e => {
                        let buttonLi = document.createElement('li');
                        let buttonA = document.createElement('a');
                        buttonA.classList.add('dropdown-item');
                        buttonA.innerText = e.name;
                        buttonLi.appendChild(buttonA);

                        constractCompanyOnClick(buttonA, `type_id=${element.id}&company_id=${e.id}`);
                        buttonList.push(buttonLi)
                    });

                    //
                    let buttonLi = document.createElement('li');
                    let buttonA = document.createElement('a');
                    let buttonUl = document.createElement('ul');
                    let buttonASpan = document.createElement('span');

                    buttonLi.id = "li";
                    buttonLi.classList.add('nav-item');
                    buttonLi.classList.add('dropdown');


                    buttonA.id = 'navbarDropdown';
                    buttonA.classList.add('nav-link');
                    buttonA.classList.add('dropdown-toggle');
                    buttonA.setAttribute('role', 'button');
                    buttonA.setAttribute('data-bs-toggle', 'dropdown');


                    buttonASpan.classList.add('span-nav');
                    buttonASpan.innerText = element.name;


                    buttonUl.classList.add('dropdown-menu');
                    buttonUl.setAttribute('aria-labelledby', 'navbarDropdown');


                    buttonLi.appendChild(buttonA);
                    buttonA.appendChild(buttonASpan);
                    buttonLi.appendChild(buttonUl);

                    buttonList.forEach(button => {
                        buttonUl.appendChild(button);

                    });


                    document.getElementById("ul").appendChild(buttonLi);



                });

            }

        })

    }

    init();


    const basketGoods = document.getElementById('basketGoods');
    //Функция для добавления товара при начальной загрузке страницы начало

    function constructGoodsList(requestElements, page) {//получает запрос для получения списка товаров с сервера

        const xhr = new XMLHttpRequest();

        xhr.open("GET", serverAddress + "api/goods?page=" + page + "&" + requestElements);
        xhr.send();

        xhr.addEventListener('readystatechange', () => {
            if (xhr.readyState == 4 && xhr.status == 200) {
                const jsonResponse = JSON.parse(xhr.response);

                if (masterGoodsDiv != null) {
                    masterGoodsDiv.remove();
                }

                masterGoodsDiv = document.createElement('div');

                masterGoodsDiv.classList.add('row');
                masterGoodsDiv.id = 'row1';

                jsonResponse.content.forEach(e => {



                    let div = document.createElement('div');

                    div.classList.add('col-sm');

                    let masterDiv = document.createElement('div');
                    let masterDivImg = document.createElement('img');
                    let masterDivChildDiv = document.createElement('div');
                    let masterDivChildDivH5 = document.createElement('h5');
                    let masterDivChildDivPDescriptoin = document.createElement('p');
                    let masterDivChildDivPPrice = document.createElement('p');
                    let masterDivChildDivPPriceSpan = document.createElement('span');
                    let masterDivChildDivPMoreAbout = document.createElement('p');
                    let masterDivChildDivPMoreAboutA = document.createElement('a');
                    let masterDivChildDivButtonBuy = document.createElement('button');




                    masterDiv.appendChild(masterDivImg);
                    masterDiv.appendChild(masterDivChildDiv);
                    masterDivChildDiv.appendChild(masterDivChildDivH5);
                    masterDivChildDiv.appendChild(masterDivChildDivPDescriptoin);
                    masterDivChildDiv.appendChild(masterDivChildDivPPrice);
                    masterDivChildDiv.appendChild(masterDivChildDivPMoreAbout);
                    masterDivChildDivPMoreAbout.appendChild(masterDivChildDivPMoreAboutA);
                    masterDivChildDiv.appendChild(masterDivChildDivButtonBuy);

                    masterDivImg.setAttribute('src', e.photoUrl);
                    masterDivChildDivH5.innerText = e.title;
                    masterDivChildDivPDescriptoin.innerText = e.description;
                    masterDivChildDivPPrice.innerHTML = `Ціна: <span class = "price">${e.price}</span> грн`;
                    masterDivChildDivPMoreAboutA.innerHTML = "Дізнатись більше>>";
                    masterDivChildDivPMoreAboutA.style.color = "#CD5C5C";
                    masterDivChildDivPMoreAboutA.style.cursor = "pointer";



                    masterDivChildDivButtonBuy.classList.add('btn');
                    masterDivChildDivButtonBuy.classList.add('btn-outline-secondary');
                    masterDivChildDivButtonBuy.setAttribute('data-bs-toggle', 'modal');
                    masterDivChildDivButtonBuy.setAttribute('data-bs-target', '#exampleModal');








                    masterDiv.classList.add('card');
                    masterDiv.style.width = "18rem";
                    masterDiv.style.marginTop = "20px";

                    masterDivImg.classList.add('card-img-top');
                    masterDivImg.setAttribute('alt', "Фото товару");
                    masterDivImg.style.width = "200px";
                    masterDivChildDiv.classList.add('card-body');
                    masterDivChildDivH5.classList.add('card-title');
                    masterDivChildDivPDescriptoin.classList.add('card-text')
                    masterDivChildDivPPrice.classList.add('card-text');
                    masterDivChildDivPMoreAbout.classList.add('card-text');
                    masterDivChildDivPPriceSpan.classList.add('price');
                    masterDivChildDivPMoreAboutA.id = "buttonAbout";
                    masterDivChildDivButtonBuy.classList.add('btn');
                    masterDivChildDivButtonBuy.classList.add('btn-outline-success');
                    masterDivChildDivButtonBuy.innerText = 'Купити'





                    div.appendChild(masterDiv);



                    masterGoodsDiv.appendChild(div);
                    document.getElementById("container").appendChild(masterGoodsDiv);

                    masterDivChildDivPMoreAboutA.addEventListener('click', () => {

                        console.log(e);
                        if (pageNav != null) {
                            pageNav.remove();

                        }
                        if (masterGoodsDiv != null) {
                            masterGoodsDiv.remove();
                        }

                        let containerAboutGoods = document.createElement('div');

                        document.getElementById('container').appendChild(containerAboutGoods);

                        containerAboutGoods.innerHTML =

                            `
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                     <img src = "${e.photoUrl}" alt = "Фото товара" style = width:400px;>
                                </div>
                                <div class="col" style = min-height:200px;min-width:200px;>
                                  ${e.description}
                                </div>
                            </div>
                        </div>    
                        `

                    });
                    // клик по кнопке КУПИТЬ
                    masterDivChildDivButtonBuy.addEventListener('click', () => {
                        getToken().then(token => {
                            requestBusket(e.id, token).then(res => getShoppingCart(token));

                        });

                    });

                });
                pageBarConstruct(jsonResponse.totalPages, requestElements)
            }
        })

    }


    
    
    
    

    //
    //         <tr>
    //           <td>Mark</td>
    //           <td>Otto</td>
    //           <td>@mdo</td>
    //         </tr>
    //       </tbody>

    async function getShoppingCart(token) {
        let response = await fetch(serverAddress + "api/user?token=" + token, {
            method: "GET",
            credentials: 'include',
        })
        const data = await response.json();
        // basketGoods.innerText = "";
        document.getElementById('tbody').innerHTML = "";
        data.shoppingCart.forEach(element => {
            
            // basketGoods.innerText += element.goods.title;
            // basketGoods.innerText +=element.goods.price;
            let tr = document.createElement('tr');

            tr.innerHTML = `
            
                <td><img id ="basketPhoto" src="${element.goods.photoUrl}" style="width:50px;"></td>
                <td>${element.goods.title}</td>
                <td>${element.goods.price}</td>
            
            ` 
            document.getElementById('tbody').appendChild(tr);

        });
      
    }


    const buttonBasket = document.getElementById('buttonBasket');

    console.log(buttonBasket);

    


    async function requestBusket(id, cookie) {
        let obj = {
            goodsId: id,
            userToken: cookie
        }
        let json = JSON.stringify(obj);

        let response = await fetch(serverAddress + "api/shopping", {
            method: 'POST',
            body: json,
            headers: {
                "Content-type": "application/json"
            }



        })

        const result = await response.json();
        return result
    }






    function constractCompanyOnClick(button, getRequestPlus) {

        button.addEventListener('click', () => {
            constructGoodsList(getRequestPlus, 0);
        });

    }

    constructGoodsList("", 0);


    let search = document.getElementById('search');
    search.addEventListener('input', () => {
        constructGoodsList(`query=${search.value}`, 0);
    })

    function pageBarConstruct(totalPageNum, requestElements) {

        if (pageNav != null) {
            pageNav.remove();
        }

        let pageButtonArray = [];
        pageNav = document.createElement('nav');
        let pageUl = document.createElement('ul');
        pageUl.id = 'page-bar-ul';
        pageUl.classList.add('pagination');
        pageUl.classList.add('justify-content-center');
        pageNav.appendChild(pageUl);
        pageUl.style.marginTop = "50px";
        pageUl.style.marginBottom = "50px";


        for (let i = 0; i < totalPageNum; i++) {

            let pageLi = document.createElement('li');
            pageLi.classList.add('page-item');
            pageUl.appendChild(pageLi);

            let pageA = document.createElement('a');
            pageA.classList.add('page-link');

            pageA.innerText = i + 1;
            pageLi.appendChild(pageA);

            pageButtonArray.push(pageA);


        }

        document.getElementById('page').appendChild(pageNav);

        constructPageOnClick(requestElements, pageButtonArray)
    }

    function constructPageOnClick(requestElements, pageButtonArray) {

        pageButtonArray.forEach(element => {

            element.addEventListener('click', () => {

                constructGoodsList(requestElements, element.innerText - 1);
                element.setAttribute('style', 'color:red;');





            });

        });


    }











});






